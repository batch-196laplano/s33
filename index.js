// expressJS API
// mock collection for courses
let courses = [
	{
		name: "Phyton 101",
		description : "Learn Phyton",
		price: 25000
	},
	{
		name: "ReactJS 101",
		description : "Learn ReactJS",
		price: 35000
	},
	{
		name: "ExpressJS 101",
		description : "Learn EpressJS",
		price: 28000
	}
];
let users =[
	{
		email: "marybell_knight",
		password: "merrymarybell"
	},
	{
		email: "janedoePriest",
		password: "jobpriest100"
	},
	{
		email: "kimTofu",
		password: "dobuTofu"
	},

]



//"express"package was imported as express
const express = require ("express")
//invoke express package to create a server/api and save it in variable which we can refer to later to create routes
const app = express();
//assign port
const port = 4000;

//dont forget this
app.use(express.json())

//used listen()of method to assign a port
	app.get('/',(req,res)=>{
		//.send() similar to end
		res.send("Hello from our first ExpressJS route");
	})

	app.post('/',(req,res)=>{
		//.send() similar to end
		res.send("Hello from our first ExpressJS Post Method Route!");
	})

	app.put('/',(req,res)=>{
		//.send() similar to end
		res.send("Hello from our first ExpressJS Put Method Route!");
	})
	app.delete('/',(req,res)=>{
		//.send() similar to end
		res.send("Hello from our first ExpressJS Delete Method Route!");
	})

app.get('/courses',(req,res)=>{
	res.send(courses);
})
app.post('/courses',(req,res)=>{
	console.log(req.body);
	courses.push(req.body);
	console.log(courses);
	res.send(courses);
})

//activity
app.get('/users',(req,res)=>{
	res.send(users);
	console.log(users);
})
app.post('/users',(req,res)=>{
	// console.log(req.body);
	users.push(req.body);
	console.log(users);
	res.send(users);
})
app.delete('/users',(req,res)=>{
	// console.log(req.body);
	users.pop(req.body);
	console.log(users);
	res.send("User deleted successfully!");
})

app.listen(port,()=> console.log(`Express API running at port 4000`))


//npm run dev runs nodemon